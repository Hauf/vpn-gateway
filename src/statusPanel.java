import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by marku on 13.02.2016.
 */
public class statusPanel extends JPanel {
    JButton restartService;
    JButton startService;
    JButton stopService;

    public statusPanel(VpnGui parent){

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(2,2,2,10);

        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        JLabel ipLabel = new JLabel("IP: ");
        gbl.setConstraints(ipLabel,gbc);
        this.add(ipLabel);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        JLabel ipText = new JLabel(SystemOperator.getIp());
        gbl.setConstraints(ipText,gbc);
        this.add(ipText);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        JLabel macLabel = new JLabel("MAC: ");
        gbl.setConstraints(macLabel,gbc);
        this.add(macLabel);
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        JLabel macText = new JLabel(SystemOperator.getMac());
        gbl.setConstraints(macText,gbc);
        this.add(macText);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        JLabel versionLabel = new JLabel("Vers.: ");
        gbl.setConstraints(versionLabel,gbc);
        this.add(versionLabel);
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        JLabel versionText = new JLabel("1.0.2");
        gbl.setConstraints(versionText,gbc);
        this.add(versionText);

        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        Icon backIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Back.png"));
        JButton back = new JButton(backIcon);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.mainPanelController(parent.currentPanel,parent.startupPanel);
                parent.currentPanel = parent.startupPanel;
            }
        });
        gbl.setConstraints(back,gbc);
        this.add(back);

        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.gridheight = 2;
        Icon restartIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Restart.png"));
        restartService = new JButton(restartIcon);
        restartService.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String output = SystemOperator.restartService();
                String search1 = "stopping";
                String search2 = "starting";
                if(output.toLowerCase().indexOf(search1.toLowerCase()) != -1 && output.toLowerCase().indexOf(search2.toLowerCase()) != -1){
                    JOptionPane.showMessageDialog(null,"Successfully restarted the VPN service","Success",JOptionPane.PLAIN_MESSAGE);
                    stopService.setEnabled(true);
                    restartService.setEnabled(true);
                    startService.setEnabled(false);
                }else {
                    JOptionPane.showMessageDialog(null,"Error restarting the VPN service"+System.lineSeparator()+output,"Error",JOptionPane.ERROR_MESSAGE);
                    restartService.setEnabled(false);
                    startService.setEnabled(true);
                    stopService.setEnabled(false);
                }
            }
        });
        gbl.setConstraints(restartService,gbc);
        this.add(restartService);

        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        Icon startIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Start.png"));
        startService = new JButton(startIcon);
        startService.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String output = SystemOperator.startService();
                String search = "starting";
                if(output.toLowerCase().indexOf(search.toLowerCase()) != -1){
                    JOptionPane.showMessageDialog(null,"Successfully started the VPN service","Success",JOptionPane.PLAIN_MESSAGE);
                    stopService.setEnabled(true);
                    restartService.setEnabled(true);
                    startService.setEnabled(false);
                }else {
                    JOptionPane.showMessageDialog(null,"Error starting the VPN service"+System.lineSeparator()+output,"Error",JOptionPane.ERROR_MESSAGE);
                    restartService.setEnabled(false);
                    startService.setEnabled(true);
                    stopService.setEnabled(false);
                }
            }
        });
        gbl.setConstraints(startService,gbc);
        this.add(startService);

        gbc.gridx = 3;
        gbc.gridy = 2;
        gbc.gridheight = 2;
        Icon stopIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Stop.png"));
        stopService = new JButton(stopIcon);
        stopService.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String output = SystemOperator.stopService();
                String search = "stopping";
                if(output.toLowerCase().indexOf(search.toLowerCase()) != -1){
                    JOptionPane.showMessageDialog(null,"Successfully stopped the VPN service","Success",JOptionPane.PLAIN_MESSAGE);
                    stopService.setEnabled(false);
                    restartService.setEnabled(false);
                    startService.setEnabled(true);
                }else {
                    JOptionPane.showMessageDialog(null,"Error stopping the VPN service"+System.lineSeparator()+output,"Error",JOptionPane.ERROR_MESSAGE);
                    restartService.setEnabled(true);
                    startService.setEnabled(false);
                    stopService.setEnabled(true);
                }
            }
        });
        gbl.setConstraints(stopService,gbc);
        this.add(stopService);

        if(SystemOperator.getVpnStatus() == "running"){
            restartService .setEnabled(true);
            startService.setEnabled(false);
            stopService.setEnabled(true);
        }else {
            restartService .setEnabled(false);
            startService.setEnabled(true);
            stopService.setEnabled(false);
        }
    }
}
