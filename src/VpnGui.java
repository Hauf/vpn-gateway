import javafx.scene.shape.Ellipse;

import javax.imageio.plugins.jpeg.JPEGHuffmanTable;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

/**
 * Created by marku on 13.02.2016.
 */
public class VpnGui extends JFrame implements Runnable {
    JPanel buttonPanel;
    JPanel mainContentPanel;
    JPanel startupPanel;
    JPanel vpnChooserPanel;
    JPanel statusPanel;
    JPanel headerPanel;
    JPanel currentPanel;
    Thread statusThread;
    JLabel status = new JLabel();
    String statusText;
    String returnStatus;
    public VpnGui(FileOperator fileOperator){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        this.setLayout(new BorderLayout(5,5));

        headerPanel = new JPanel();
        headerPanel.setLayout(new FlowLayout(FlowLayout.RIGHT,5,5));
        statusThread = new Thread(this);
        statusThread.start();
        headerPanel.add(status);
        headerPanel.add(new JLabel("                             "));

        Icon reloadIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Restart.png"));
        JButton reloadStatus = new JButton(reloadIcon);
        reloadStatus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                returnStatus = SystemOperator.getVpnStatus();
                statusText = returnStatus;
                if (returnStatus == "running"){
                    status.setForeground(Color.green);
                } else {
                    status.setForeground(Color.red);
                }
                status.setText(statusText);
            }
        });
        headerPanel.add(reloadStatus);

        this.add(headerPanel,BorderLayout.NORTH);

        mainContentPanel = new JPanel();
        //prepare startup panel
        startupPanel = new JPanel();
        Icon country = fileOperator.getCountry();
        JLabel welcome = new JLabel(country);
        startupPanel.add(welcome);

        //prepare vpn chooser panel
        vpnChooserPanel = new vpnChooserPanel(this);

        //prepare status panel
        statusPanel = new statusPanel(this);

        mainContentPanel.add(startupPanel);
        currentPanel = startupPanel;

        this.add(mainContentPanel, BorderLayout.CENTER);

        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridLayout(2,2,3,3));
        JButton changeCountryButton = new JButton("Change Country");
        changeCountryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainPanelController(currentPanel, vpnChooserPanel);
                currentPanel = vpnChooserPanel;
            }
        });

        changeCountryButton.setEnabled(false);

        JButton statusButton = new JButton("Service");
        statusButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainPanelController(currentPanel,statusPanel);
                currentPanel = statusPanel;
            }
        });
        JButton restartButton = new JButton("Restart");
        restartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"Yes", "No"};
                int option = JOptionPane.showOptionDialog(null, "Do you want to restart the system?", "Restart",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);
                if (option == JOptionPane.YES_OPTION){
                    SystemOperator.restartSystem();
                }else if(option == JOptionPane.NO_OPTION){
                    System.out.println("no");
                }
            }
        });
        JButton shutdownButton = new JButton("Shutdown");
        shutdownButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] options = {"Yes", "No"};
                int option = JOptionPane.showOptionDialog(null, "Do you want to shutdown the system?", "Shutdown",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null, options, options[0]);
                if (option == JOptionPane.YES_OPTION){
                    SystemOperator.shutdownSystem();
                }else if(option == JOptionPane.NO_OPTION){
                    System.out.println("no");
                }
            }
        });
        buttonPanel.add(changeCountryButton);
        buttonPanel.add(statusButton);
        buttonPanel.add(restartButton);
        buttonPanel.add(shutdownButton);
        this.add(buttonPanel, BorderLayout.SOUTH);


        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(320,240);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
    }

    public void mainPanelController(JPanel toRemove, JPanel toAdd){
        mainContentPanel.remove(toRemove);
        mainContentPanel.add(toAdd);
        mainContentPanel.validate();
        mainContentPanel.repaint();
    }

    @Override
    public void run() {
        while (true){
            try {
                returnStatus = SystemOperator.getVpnStatus();
                statusText = returnStatus;
                if (returnStatus == "running"){
                    status.setForeground(Color.green);
                } else {
                    status.setForeground(Color.red);
                }
                status.setText(statusText);
                Thread.sleep(300000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
