import java.io.*;

/**
 * Created by marku on 13.02.2016.
 */
public class SystemOperator {

    public static String getVpnStatus(){

        String onlineStatus = "not running";
        String output = runCommand("ifconfig");
        String search = "tun0";
        if(output.toLowerCase().indexOf(search.toLowerCase()) != -1){
            onlineStatus = "running";
        }

        return onlineStatus;
    }

    public static String getIp(){
        String ip = "127.0.0.1";
        String output = runCommand("ifconfig");
        String search = "inet addr:";
        int index = output.toLowerCase().indexOf(search.toLowerCase());
        index += 10;
        ip = output.substring(index,index+13);
        return ip;
    }

    public static String getMac(){
        String mac = "aaaa";
        String output = runCommand("ifconfig");
        String search = "HWaddr ";
        int index = output.toLowerCase().indexOf(search.toLowerCase());
        index += 7;
        mac = output.substring(index,index+17);
        return mac;
    }

    public static String runCommand(String command){
        String output = "";
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(command);
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;

            while ((line = br.readLine()) != null) {
                output += (line + " ");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return output;
    }

    public static void restartSystem(){
        try {
            Runtime.getRuntime().exec("sudo reboot");
        } catch (IOException e) {

        }
    }
    public static void shutdownSystem(){
        try {
            Runtime.getRuntime().exec("sudo shutdown now");
        } catch (IOException e) {

        }
    }
    public static String restartService(){
        String output = runCommand("sudo service openvpn restart");
        return output;
    }
    public static String startService(){
        String output = runCommand("sudo service openvpn start");
        return output;
    }
    public static String stopService(){
        String output = runCommand("sudo service openvpn stop");
        return output;
    }
}
