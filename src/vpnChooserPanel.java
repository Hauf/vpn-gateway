import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by marku on 13.02.2016.
 */
public class vpnChooserPanel extends JPanel {

    public vpnChooserPanel(VpnGui parent){
        this.setLayout(new FlowLayout());
        Icon backIcon = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/Back.png"));
        JButton back = new JButton(backIcon);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                parent.mainPanelController(parent.currentPanel,parent.startupPanel);
                parent.currentPanel = parent.startupPanel;
            }
        });
        this.add(back);
    }
}
