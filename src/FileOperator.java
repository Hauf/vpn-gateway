import javax.swing.*;
import java.io.File;

/**
 * Created by marku on 13.02.2016.
 */
public class FileOperator {
    File openvpn;

    public FileOperator() {
        this.openvpn = new File("/etc/default/openvpn");
    }

    public Icon getCountry(){
        Icon country = new ImageIcon(getClass().getClassLoader()
                .getResource("Icons/ger.gif"));
        return country;
    }
}
